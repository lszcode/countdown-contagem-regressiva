from tkinter import *

def ClickTime(valid = False,sec = None):
    if valid == False:
        sec = int(ent.get())
    if sec == 0:
        time['text'] = 'Timeout!!'
    else:
        sec -= 1
        time['text'] = sec
        time.after(1000, lambda : ClickTime(True,sec))

root = Tk()

lb = Label(root, text='Time:')
lb.place(x=20,y=20)

ent = Entry(root, width=10)
ent.place(x=70,y=20)

btn = Button(root, text='START', width=5, fg='green', command=ClickTime)
btn.place(x=65,y=80)

time = Label(root, text='00', fg='blue')
time.place(x=80,y=130)

root.title('Regressive')
root.geometry('200x200')
root.mainloop()